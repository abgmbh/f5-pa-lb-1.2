resource "azurerm_network_interface" "nic-06" {
  name                = "${var.prefix}-nic-06"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s0.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-06
    public_ip_address_id          = azurerm_public_ip.pip-06.id
  }

  tags = {
    Name        = "${var.environment}-nic-06"
    application = var.application
  }
}

resource "azurerm_network_interface" "nic-07" {
  name                = "${var.prefix}-nic-07"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s0.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-07
    public_ip_address_id          = azurerm_public_ip.pip-07.id
  }

  tags = {
    Name        = "${var.environment}-nic-07"
  }
}

resource "azurerm_network_interface" "nic-114" {
  name                 = "${var.prefix}-nic-114"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s11.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-114
    primary                       = true
  }

  tags = {
    Name                      = "${var.environment}-ip-114"
  }
}

resource "azurerm_network_interface" "nic-124" {
  name                 = "${var.prefix}-nic-124"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s12.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-124
    primary                       = true
  }

  tags = {
    Name                      = "${var.environment}-nic-124"
  }
}

resource "azurerm_network_interface" "nic-134" {
  name                 = "${var.prefix}-nic-134"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s13.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-134
    primary                       = true
  }

  tags = {
    Name                      = "${var.environment}-nic-134"
  }
}

resource "azurerm_network_interface" "nic-144" {
  name                 = "${var.prefix}-nic-144"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s14.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-144
    primary                       = true
  }

  tags = {
    Name                      = "${var.environment}-nic-144"
  }
}

resource "azurerm_network_interface" "nic-74" {
  name                 = "${var.prefix}-nic-74"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s7.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-74
    primary                       = true
  }

  tags = {
    Name                      = "${var.environment}-nic-74"
  }
}

resource "azurerm_network_interface" "nic-94" {
  name                 = "${var.prefix}-nic-94"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s9.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-94
    primary                       = true
  }

  tags = {
    Name                      = "${var.environment}-nic-94"
  }
}

resource "azurerm_network_interface" "nic-84" {
  name                 = "${var.prefix}-nic-84"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s8.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-84
    primary                       = true
  }

  tags = {
    Name                      = "${var.environment}-nic-84"
  }
}

resource "azurerm_network_interface" "nic-104" {
  name                 = "${var.prefix}-nic-104"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s10.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-104
    primary                       = true
  }

  tags = {
    Name                      = "${var.environment}-nic-104"
  }
}

# Associate network security groups with NICs
resource "azurerm_network_interface_security_group_association" "nsg-06" {
  network_interface_id      = azurerm_network_interface.nic-06.id
  network_security_group_id = azurerm_network_security_group.nsg-0.id
}

resource "azurerm_network_interface_security_group_association" "nsg-07" {
  network_interface_id      = azurerm_network_interface.nic-07.id
  network_security_group_id = azurerm_network_security_group.nsg-0.id
}

resource "azurerm_network_interface_security_group_association" "nsg-114" {
  network_interface_id      = azurerm_network_interface.nic-114.id
  network_security_group_id = azurerm_network_security_group.nsg-114134124144.id
}

resource "azurerm_network_interface_security_group_association" "nsg-124" {
  network_interface_id      = azurerm_network_interface.nic-124.id
  network_security_group_id = azurerm_network_security_group.nsg-114134124144.id
}

resource "azurerm_network_interface_security_group_association" "nsg-134" {
  network_interface_id      = azurerm_network_interface.nic-134.id
  network_security_group_id = azurerm_network_security_group.nsg-114134124144.id
}

resource "azurerm_network_interface_security_group_association" "nsg-144" {
  network_interface_id      = azurerm_network_interface.nic-144.id
  network_security_group_id = azurerm_network_security_group.nsg-114134124144.id
}

resource "azurerm_network_interface_security_group_association" "nsg-74" {
  network_interface_id      = azurerm_network_interface.nic-74.id
  network_security_group_id = azurerm_network_security_group.nsg-749484104.id
}

resource "azurerm_network_interface_security_group_association" "nsg-84" {
  network_interface_id      = azurerm_network_interface.nic-84.id
  network_security_group_id = azurerm_network_security_group.nsg-749484104.id
}

resource "azurerm_network_interface_security_group_association" "nsg-94" {
  network_interface_id      = azurerm_network_interface.nic-94.id
  network_security_group_id = azurerm_network_security_group.nsg-749484104.id
}

resource "azurerm_network_interface_security_group_association" "nsg-104" {
  network_interface_id      = azurerm_network_interface.nic-104.id
  network_security_group_id = azurerm_network_security_group.nsg-749484104.id
}