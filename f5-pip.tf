# Create Public IPs
resource "azurerm_public_ip" "pip-04" {
  name                = "${var.prefix}-pip-04"
  location            = azurerm_resource_group.main.location
  sku                 = "Standard"
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Static"

  tags = {
    Name        = "${var.environment}-pip-04"
  }
}

resource "azurerm_public_ip" "pip-05" {
  name                = "${var.prefix}-pip-05"
  location            = azurerm_resource_group.main.location
  sku                 = "Standard"
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Static"

  tags = {
    Name        = "${var.environment}-pip-05"
  }
}

resource "azurerm_public_ip" "pip-06" {
  name                = "${var.prefix}-pip-06"
  location            = azurerm_resource_group.main.location
  sku                 = "Standard"
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Static"

  tags = {
    Name        = "${var.environment}-pip-06"
  }
}

resource "azurerm_public_ip" "pip-07" {
  name                = "${var.prefix}-pip-07"
  location            = azurerm_resource_group.main.location
  sku                 = "Standard"
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Static"

  tags = {
    Name        = "${var.environment}-pip-07"
  }
}

resource "azurerm_public_ip" "pip-24" {
  name                = "${var.prefix}-pip-24"
  location            = azurerm_resource_group.main.location
  sku                 = "Standard"
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Static"

  tags = {
    Name        = "${var.environment}-pip-24"
  }
}

resource "azurerm_public_ip" "pip-14" {
  name                = "${var.prefix}-pip-14"
  location            = azurerm_resource_group.main.location
  sku                 = "Standard"
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Static"

  tags = {
    Name        = "${var.environment}-pip-14"
  }
}