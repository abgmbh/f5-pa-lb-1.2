# Create a Network Security Group and rules
resource "azurerm_network_security_group" "nsg-749484104" {
  name                = "${var.prefix}-nsg-749484104"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  security_rule {
    name                       = "allow_HTTP"
    description                = "Allow HTTP access"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefixes      = ["0.0.0.0/0"]
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow_HTTPS"
    description                = "Allow HTTPS access"
    priority                   = 120
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefixes      = ["0.0.0.0/0"]
    destination_address_prefix = "*"
  }

  tags = {
    Name        = "${var.environment}-nsg-749484104"
  }
}

resource "azurerm_network_security_group" "nsg-114134124144" {
  name                = "${var.prefix}-nsg-114134124144"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

security_rule {
    name                       = "allow_all"
    description                = "Allow all"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefixes      = ["0.0.0.0/0"]
    destination_address_prefix = "*"
  }

  tags = {
    Name        = "${var.environment}-nsg-114134124144"
  }
}