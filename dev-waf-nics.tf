resource "azurerm_network_interface" "nic-d-04" {
  name                = "${var.prefix}-nic-d-04"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.ds0.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-d-04
    public_ip_address_id          = azurerm_public_ip.pip-d-04.id

  }

  tags = {
    Name = "${var.environment}-nic-d-04"
  }
}

resource "azurerm_network_interface" "nic-d-14" {
  name                 = "${var.prefix}-nic-d-14"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.ds1.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-d-14
    primary                       = true
  }

  tags = {
    Name = "${var.environment}-nic-d-14"
  }
}

resource "azurerm_network_interface" "nic-d-24" {
  name                 = "${var.prefix}-nic-d-24"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.ds2.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-d-24
    primary                       = true
  }

  tags = {
    Name = "${var.environment}-nic-d-24"
  }
}

resource "azurerm_network_interface" "nic-d-34" {
  name                 = "${var.prefix}-nic-d-34"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.ds3.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-d-34
  }

  tags = {
    Name = "${var.environment}-nic-d-34"
  }
}

resource "azurerm_network_interface_security_group_association" "nsg-d-04" {
  network_interface_id      = azurerm_network_interface.nic-d-04.id
  network_security_group_id = azurerm_network_security_group.nsg-0.id
}

resource "azurerm_network_interface_security_group_association" "nsg-d-14" {
  network_interface_id      = azurerm_network_interface.nic-d-14.id
  network_security_group_id = azurerm_network_security_group.nsg-d-1424.id
}

resource "azurerm_network_interface_security_group_association" "nsg-d-24" {
  network_interface_id      = azurerm_network_interface.nic-d-24.id
  network_security_group_id = azurerm_network_security_group.nsg-d-1424.id
}

resource "azurerm_network_interface_security_group_association" "nsg-d-34" {
  network_interface_id      = azurerm_network_interface.nic-d-34.id
  network_security_group_id = azurerm_network_security_group.nsg-d-34.id
}
