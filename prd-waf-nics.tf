resource "azurerm_network_interface" "nic-p-04" {
  name                = "${var.prefix}-nic-p-04"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.ps0.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-p-04
    public_ip_address_id          = azurerm_public_ip.pip-p-04.id

  }

  tags = {
    Name        = "${var.environment}-nic-p-04"
    application = var.application
  }
}

resource "azurerm_network_interface" "nic-p-14" {
  name                 = "${var.prefix}-nic-p-14"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.ps1.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-p-14
    primary                       = true
  }

  tags = {
    Name = "${var.environment}-nic-p-14"
  }
}

resource "azurerm_network_interface" "nic-p-24" {
  name                 = "${var.prefix}-nic-p-24"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.ps2.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-p-24
    primary                       = true
  }

  tags = {
    Name = "${var.environment}-nic-p-24"
  }
}

resource "azurerm_network_interface" "nic-p-34" {
  name                 = "${var.prefix}-nic-p-34"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.ps3.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-p-34
  }

  tags = {
    Name = "${var.environment}-nic-p-34"
  }
}

resource "azurerm_network_interface_security_group_association" "nsg-p-04" {
  network_interface_id      = azurerm_network_interface.nic-p-04.id
  network_security_group_id = azurerm_network_security_group.nsg-0.id
}

resource "azurerm_network_interface_security_group_association" "nsg-p-14" {
  network_interface_id      = azurerm_network_interface.nic-p-14.id
  network_security_group_id = azurerm_network_security_group.nsg-p-1424.id
}

resource "azurerm_network_interface_security_group_association" "nsg-p-24" {
  network_interface_id      = azurerm_network_interface.nic-p-24.id
  network_security_group_id = azurerm_network_security_group.nsg-p-1424.id
}

resource "azurerm_network_interface_security_group_association" "nsg-p-34" {
  network_interface_id      = azurerm_network_interface.nic-p-34.id
  network_security_group_id = azurerm_network_security_group.nsg-p-34.id
}
