# Create F5 BIG-IP VMs

resource "azurerm_marketplace_agreement" "f5" {
  publisher = "f5-networks"
  offer     = var.product
  plan      = var.plan
}

resource "azurerm_linux_virtual_machine" "f501-vm" {
  name                            = "${var.prefix}-f501-vm"
  location                        = azurerm_resource_group.main.location
  resource_group_name             = azurerm_resource_group.main.name
  network_interface_ids           = [azurerm_network_interface.nic-04.id, azurerm_network_interface.nic-14.id, azurerm_network_interface.nic-34.id, azurerm_network_interface.nic-54.id]
  size                            = var.instance-type
  admin_username                  = var.username
  disable_password_authentication = true
  computer_name                   = "${var.prefix}-f501-vm"
  custom_data                     = base64encode(data.template_file.vm_onboard.rendered)

  os_disk {
    name                 = "${var.prefix}-f501-vm-osdisk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  admin_ssh_key {
    username   = var.username
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    publisher = "f5-networks"
    offer     = var.product
    sku       = var.plan
    version   = var.bigip-version
  }

  plan {
    name      = var.plan
    publisher = "f5-networks"
    product   = var.product
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.mystorage.primary_blob_endpoint
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    Name = "${var.environment}-f501-vm"
  }
}

resource "azurerm_linux_virtual_machine" "f502-vm" {
  name                            = "${var.prefix}-f502-vm"
  location                        = azurerm_resource_group.main.location
  resource_group_name             = azurerm_resource_group.main.name
  network_interface_ids           = [azurerm_network_interface.nic-05.id, azurerm_network_interface.nic-24.id, azurerm_network_interface.nic-44.id, azurerm_network_interface.nic-64.id]
  size                            = var.instance-type
  admin_username                  = var.username
  disable_password_authentication = true
  computer_name                   = "${var.prefix}-f502-vm"
  custom_data                     = base64encode(data.template_file.vm_onboard.rendered)

  os_disk {
    name                 = "${var.prefix}-f502-vm-osdisk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  admin_ssh_key {
    username   = var.username
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    publisher = "f5-networks"
    offer     = var.product
    sku       = var.plan
    version   = var.bigip-version
  }

  plan {
    name      = var.plan
    publisher = "f5-networks"
    product   = var.product
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.mystorage.primary_blob_endpoint
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    Name        = "${var.environment}-f502-vm"
    environment = var.environment
    owner       = var.owner
    group       = var.group
    costcenter  = var.costcenter
    application = var.application
  }
}

# Run Startup Script
resource "azurerm_virtual_machine_extension" "f501-vm-run-startup-cmd" {
  name                 = "${var.environment}-f501-vm-run-startup-cmd"
  virtual_machine_id   = azurerm_linux_virtual_machine.f501-vm.id
  publisher            = "Microsoft.Azure.Extensions"
  type                 = "CustomScript"
  type_handler_version = "2.0"

  settings = <<SETTINGS
    {
        "commandToExecute": "bash /var/lib/waagent/CustomData; exit 0;"
    }
  SETTINGS

  tags = {
    Name = "${var.environment}-f501-vm-startup-cmd"
  }
}

resource "azurerm_virtual_machine_extension" "f502-vm-run-startup-cmd" {
  name                 = "${var.environment}-f502-vm-run-startup-cmd"
  virtual_machine_id   = azurerm_linux_virtual_machine.f502-vm.id
  publisher            = "Microsoft.Azure.Extensions"
  type                 = "CustomScript"
  type_handler_version = "2.0"

  settings = <<SETTINGS
    {
        "commandToExecute": "bash /var/lib/waagent/CustomData; exit 0;"
    }
  SETTINGS

  tags = {
    Name = "${var.environment}-f502-vm-startup-cmd"
  }
}
