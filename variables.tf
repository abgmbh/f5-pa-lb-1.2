# REST API Setting
variable "rest-do-uri" { default = "/mgmt/shared/declarative-onboarding" }
variable "rest-as3-uri" { default = "/mgmt/shared/appsvcs/declare" }
variable "rest-do-method" { default = "POST" }
variable "rest-as3-method" { default = "POST" }
variable "rest-vm01-do-file" { default = "vm01-do-data.json" }
variable "rest-vm02-do-file" { default = "vm02-do-data.json" }
variable "rest-vm-as3-file" { default = "vm-as3-data.json" }
variable "rest-ts-uri" { default = "/mgmt/shared/telemetry/declare" }
variable "rest-vm-ts-file" { default = "vm-ts-data.json" }
variable "rest-CF-uri" { default = "/mgmt/shared/cloud-failover/declare" }
variable "rest-vm-failover-file" { default = "vm-failover-data.json" }

variable "vm-onboard-file" { default = "vm-onboard-data.sh" }

# Azure Environment
variable "prefix" {}
variable "username" {}
variable "password" {}
variable "azure_client_id" {}
variable "azure_client_secret" {}
variable "azure_subscription_id" {}
variable "azure_tenant_id" {}
variable "location" {

}
variable "me" {}


# NETWORK
variable "cidr" { default = "10.0.0.0/16" }
variable "prd-cidr" { default = "10.1.0.0/16" }
variable "dev-cidr" { default = "10.2.0.0/16" }


variable "subnets" {
  type = map(any)
  default = {
    "s0"  = "10.0.0.0/24"
    "s1"  = "10.0.1.0/24"
    "s2"  = "10.0.2.0/24"
    "s3"  = "10.0.3.0/24"
    "s4"  = "10.0.4.0/24"
    "s5"  = "10.0.5.0/24"
    "s6"  = "10.0.6.0/24"
    "s7"  = "10.0.7.0/24"
    "s8"  = "10.0.8.0/24"
    "s9"  = "10.0.9.0/24"
    "s10" = "10.0.10.0/24"
    "s11" = "10.0.11.0/24"
    "s12" = "10.0.12.0/24"
    "s13" = "10.0.13.0/24"
    "s14" = "10.0.14.0/24"
  }
}

variable "prd-subnets" {
  type = map(any)
  default = {
    "ps0" = "10.1.0.0/24"
    "ps1" = "10.1.1.0/24"
    "ps2" = "10.1.2.0/24"
    "ps3" = "10.1.3.0/24"
    "ps4" = "10.1.4.0/24"

  }
}

variable "dev-subnets" {
  type = map(any)
  default = {
    "ds0" = "10.2.0.0/24"
    "ds1" = "10.2.1.0/24"
    "ds2" = "10.2.2.0/24"
    "ds3" = "10.2.3.0/24"
    "ds4" = "10.2.4.0/24"

  }
}

variable "ip-04" { default = "10.0.0.4" }
variable "ip-05" { default = "10.0.0.5" }
variable "ip-06" { default = "10.0.0.6" }
variable "ip-07" { default = "10.0.0.7" }
variable "ip-14" { default = "10.0.1.4" }
variable "ip-24" { default = "10.0.2.4" }
variable "ip-34" { default = "10.0.3.4" }
variable "ip-44" { default = "10.0.4.4" }
variable "ip-54" { default = "10.0.5.4" }
variable "ip-64" { default = "10.0.6.4" }
variable "ip-74" { default = "10.0.7.4" }
variable "ip-84" { default = "10.0.8.4" }
variable "ip-94" { default = "10.0.9.4" }
variable "ip-104" { default = "10.0.10.4" }
variable "ip-114" { default = "10.0.11.4" }
variable "ip-124" { default = "10.0.12.4" }
variable "ip-134" { default = "10.0.13.4" }
variable "ip-144" { default = "10.0.14.4" }

variable "mgmt-gw" { default = "10.0.0.1" }

variable "ip-p-04" { default = "10.1.0.4" }
variable "ip-p-05" { default = "10.1.0.5" }
variable "ip-p-14" { default = "10.1.1.4" }
variable "ip-p-24" { default = "10.1.2.4" }
variable "ip-p-34" { default = "10.1.3.4" }
variable "ip-p-44" { default = "10.1.4.4" }

variable "ip-d-04" { default = "10.2.0.4" }
variable "ip-d-05" { default = "10.2.0.5" }
variable "ip-d-14" { default = "10.2.1.4" }
variable "ip-d-24" { default = "10.2.2.4" }
variable "ip-d-34" { default = "10.2.3.4" }
variable "ip-d-44" { default = "10.2.4.4" }


# BIG-IP Image
variable "instance-type" { default = "Standard_DS4_v2" }
variable "plan" { default = "f5-bigip-virtual-edition-25m-best-hourly-po-f5" }
variable "product" { default = "f5-big-ip-best" }
variable "bigip-version" { default = "latest" }

# BIG-IP Setup
variable "license1" { default = "" }
variable "license2" { default = "" }
variable "host1-name" { default = "f5vm01" }
variable "host2-name" { default = "f5vm02" }
variable "dns-server" { default = "8.8.8.8" }
variable "ntp-server" { default = "0.us.pool.ntp.org" }
variable "timezone" { default = "UTC" }
variable "DO-URL" { default = "https://github.com/F5Networks/f5-declarative-onboarding/releases/download/v1.15.0/f5-declarative-onboarding-1.15.0-3.noarch.rpm" }
variable "AS3-URL" { default = "https://github.com/F5Networks/f5-appsvcs-extension/releases/download/v3.21.0/f5-appsvcs-3.21.0-4.noarch.rpm" }
variable "TS-URL" { default = "https://github.com/F5Networks/f5-telemetry-streaming/releases/download/v1.13.0/f5-telemetry-1.13.0-2.noarch.rpm" }
variable "CF-URL" { default = "https://github.com/F5Networks/f5-cloud-failover-extension/releases/download/v1.4.0/f5-cloud-failover-1.4.0-0.noarch.rpm" }
variable "libs-dir" { default = "/config/cloud/azure/node-modules" }
variable "onboard-log" { default = "/var/log/startup-script.log" }

# TAGS
variable "purpose" { default = "public" }
variable "environment" { default = "f5env" } #ex. dev/staging/prod
variable "owner" { default = "f5owner" }
variable "group" { default = "f5group" }
variable "costcenter" { default = "f5costcenter" }
variable "application" { default = "f5app" }
variable "f5-cloud-failover-label" { default = "mydeployment" } #Cloud Failover Tag
variable "f5-cloud-failover-nic-map" { default = "external" }   #NIC Tag

# PA FW
variable "FirewallVmSize" { default = "Standard_DS4_v2" }
variable "fwSku" { default = "bundle2" }
variable "fwOffer" { default = "vmseries1" }
variable "fwPublisher" { default = "paloaltonetworks" }
variable "adminUsername" { default = "paloalto" }
variable "adminPassword" { default = "Pal0Alt0@123" }
variable "FirewallVmName" { default = "vvPANW" }

# BE
variable "b-instance-type" { default = "Standard_B1ms" }
