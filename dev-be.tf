resource "azurerm_network_interface" "nic-d-44" {
  name                = "${var.prefix}-nic-d-44"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.ds4.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-d-44
  }

  tags = {
    Name = "${var.environment}-nic-d-44"
  }
}

resource "azurerm_network_interface" "nic-d-05" {
  name                = "${var.prefix}-nic-d-05"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.ds0.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-d-05
    public_ip_address_id          = azurerm_public_ip.pip-d-05.id

  }

  tags = {
    Name        = "${var.environment}-nic-d-05"
    application = var.application
  }
}

resource "azurerm_network_security_group" "nsg-d-44" {
  name                = "${var.prefix}-nsg-d-44"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  security_rule {
    name                       = "allow_all"
    description                = "Allow all"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefixes    = ["0.0.0.0/0"]
    destination_address_prefix = "*"
  }

  tags = {
    Name = "${var.environment}-nsg-d-44"
  }
}

resource "azurerm_network_security_group" "nsg-d-05" {
  name                = "${var.prefix}-nsg-d-05"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  security_rule {
    name                       = "allow_SSH"
    description                = "Allow SSH access"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefixes    = [var.me]
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow_HTTPS"
    description                = "Allow HTTPS access"
    priority                   = 120
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefixes    = [var.me]
    destination_address_prefix = "*"
  }

  tags = {
    Name = "${var.environment}-nsg-d-44"
  }
}

resource "azurerm_public_ip" "pip-d-05" {
  name                = "${var.prefix}-pip-d-05"
  location            = azurerm_resource_group.main.location
  sku                 = "Standard"
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Static"

  tags = {
    Name = "${var.environment}-pip-d-05"
  }
}

resource "azurerm_network_interface_security_group_association" "nsg-d-44" {
  network_interface_id      = azurerm_network_interface.nic-d-44.id
  network_security_group_id = azurerm_network_security_group.nsg-d-44.id
}

resource "azurerm_network_interface_security_group_association" "nsg-d-05" {
  network_interface_id      = azurerm_network_interface.nic-d-05.id
  network_security_group_id = azurerm_network_security_group.nsg-d-05.id
}

resource "azurerm_linux_virtual_machine" "d-be-vm" {
  name                = "${var.prefix}-d-be-vm"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  network_interface_ids = [azurerm_network_interface.nic-d-05.id,
    azurerm_network_interface.nic-d-44.id
  ]
  size                            = var.b-instance-type
  admin_username                  = var.username
  disable_password_authentication = true
  computer_name                   = "${var.prefix}-d-be-vm"

  os_disk {
    name                 = "${var.prefix}-d-be-vm-osdisk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  admin_ssh_key {
    username   = var.username
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.mystorage.primary_blob_endpoint
  }

  tags = {
    Name = "${var.environment}-d-be-vm"
  }
}


