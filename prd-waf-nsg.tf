resource "azurerm_network_security_group" "nsg-p-1424" {
  name                = "${var.prefix}-nsg-p-1424"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  security_rule {
    name                       = "allow_all"
    description                = "allow_all"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefixes      = ["0.0.0.0/0"]
    destination_address_prefix = "*"
  }

  tags = {
    Name        = "${var.environment}-nsg-p-1424"
  }
}

resource "azurerm_network_security_group" "nsg-p-34" {
  name                = "${var.prefix}-nsg-p-34"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  security_rule {
    name                       = "allow_all"
    description                = "allow_all"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefixes      = ["0.0.0.0/0"]
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow_all_o"
    description                = "allow_all_o"
    priority                   = 110
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefixes      = ["0.0.0.0/0"]
    destination_address_prefix = "*"
  }

  tags = {
    Name        = "${var.environment}-nsg-p-34"
  }
}