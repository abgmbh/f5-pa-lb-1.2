# Create the VNET and subnets
resource "azurerm_virtual_network" "dev" {
  name                = "${var.prefix}-dev"
  address_space       = [var.dev-cidr]
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
}

resource "azurerm_subnet" "ds0" {
  name                 = "ds0"
  virtual_network_name = azurerm_virtual_network.dev.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.dev-subnets["ds0"]]
}

resource "azurerm_subnet" "ds1" {
  name                 = "ds1"
  virtual_network_name = azurerm_virtual_network.dev.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.dev-subnets["ds1"]]
}

resource "azurerm_subnet" "ds2" {
  name                 = "ds2"
  virtual_network_name = azurerm_virtual_network.dev.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.dev-subnets["ds2"]]
}

resource "azurerm_subnet" "ds3" {
  name                 = "ds3"
  virtual_network_name = azurerm_virtual_network.dev.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.dev-subnets["ds3"]]
}

resource "azurerm_subnet" "ds4" {
  name                 = "ds4"
  virtual_network_name = azurerm_virtual_network.dev.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.dev-subnets["ds4"]]
}