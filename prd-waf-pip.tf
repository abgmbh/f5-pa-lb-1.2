resource "azurerm_public_ip" "pip-p-04" {
  name                = "${var.prefix}-pip-p-04"
  location            = azurerm_resource_group.main.location
  sku                 = "Standard"
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Static"

  tags = {
    Name        = "${var.environment}-pip-p-04"
  }
}