# Create the VNET and subnets
resource "azurerm_virtual_network" "prd" {
  name                = "${var.prefix}-prd"
  address_space       = [var.prd-cidr]
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
}

resource "azurerm_subnet" "ps0" {
  name                 = "ps0"
  virtual_network_name = azurerm_virtual_network.prd.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.prd-subnets["ps0"]]
}

resource "azurerm_subnet" "ps1" {
  name                 = "ps1"
  virtual_network_name = azurerm_virtual_network.prd.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.prd-subnets["ps1"]]
}

resource "azurerm_subnet" "ps2" {
  name                 = "ps2"
  virtual_network_name = azurerm_virtual_network.prd.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.prd-subnets["ps2"]]
}

resource "azurerm_subnet" "ps3" {
  name                 = "ps3"
  virtual_network_name = azurerm_virtual_network.prd.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.prd-subnets["ps3"]]
}

resource "azurerm_subnet" "ps4" {
  name                 = "ps4"
  virtual_network_name = azurerm_virtual_network.prd.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.prd-subnets["ps4"]]
}


