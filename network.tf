# Create the VNET and subnets
resource "azurerm_virtual_network" "main" {
  name                = "${var.prefix}-network"
  address_space       = [var.cidr]
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
}

resource "azurerm_subnet" "s0" {
  name                 = "s0"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s0"]]
}

resource "azurerm_subnet" "s1" {
  name                 = "s1"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s1"]]
}

resource "azurerm_subnet" "s2" {
  name                 = "s2"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s2"]]
}

resource "azurerm_subnet" "s3" {
  name                 = "s3"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s3"]]
}

resource "azurerm_subnet" "s4" {
  name                 = "s4"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s4"]]
}

resource "azurerm_subnet" "s5" {
  name                 = "s5"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s5"]]
}

resource "azurerm_subnet" "s6" {
  name                 = "s6"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s6"]]
}

resource "azurerm_subnet" "s7" {
  name                 = "s7"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s7"]]
}

resource "azurerm_subnet" "s8" {
  name                 = "s8"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s8"]]
}
resource "azurerm_subnet" "s9" {
  name                 = "s9"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s9"]]
}

resource "azurerm_subnet" "s10" {
  name                 = "s10"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s10"]]
}

resource "azurerm_subnet" "s11" {
  name                 = "s11"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s11"]]
}

resource "azurerm_subnet" "s12" {
  name                 = "s12"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s12"]]
}

resource "azurerm_subnet" "s13" {
  name                 = "s13"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s13"]]
}

resource "azurerm_subnet" "s14" {
  name                 = "s14"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.subnets["s14"]]
}


