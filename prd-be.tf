resource "azurerm_network_interface" "nic-p-44" {
  name                = "${var.prefix}-nic-p-44"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.ps4.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-p-44
  }

  tags = {
    Name = "${var.environment}-nic-p-44"
  }
}

resource "azurerm_network_interface" "nic-p-05" {
  name                = "${var.prefix}-nic-p-05"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.ps0.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-p-05
    public_ip_address_id          = azurerm_public_ip.pip-p-05.id

  }

  tags = {
    Name        = "${var.environment}-nic-p-05"
    application = var.application
  }
}

resource "azurerm_network_security_group" "nsg-p-44" {
  name                = "${var.prefix}-nsg-p-44"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  security_rule {
    name                       = "allow_all"
    description                = "Allow all"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefixes    = ["0.0.0.0/0"]
    destination_address_prefix = "*"
  }

  tags = {
    Name = "${var.environment}-nsg-p-44"
  }
}

resource "azurerm_network_security_group" "nsg-p-05" {
  name                = "${var.prefix}-nsg-p-05"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  security_rule {
    name                       = "allow_SSH"
    description                = "Allow SSH access"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefixes    = [var.me]
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow_HTTPS"
    description                = "Allow HTTPS access"
    priority                   = 120
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefixes    = [var.me]
    destination_address_prefix = "*"
  }

  tags = {
    Name = "${var.environment}-nsg-p-44"
  }
}

resource "azurerm_public_ip" "pip-p-05" {
  name                = "${var.prefix}-pip-p-05"
  location            = azurerm_resource_group.main.location
  sku                 = "Standard"
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Static"

  tags = {
    Name = "${var.environment}-pip-p-05"
  }
}

resource "azurerm_network_interface_security_group_association" "nsg-p-44" {
  network_interface_id      = azurerm_network_interface.nic-p-44.id
  network_security_group_id = azurerm_network_security_group.nsg-p-44.id
}

resource "azurerm_network_interface_security_group_association" "nsg-p-05" {
  network_interface_id      = azurerm_network_interface.nic-p-05.id
  network_security_group_id = azurerm_network_security_group.nsg-p-05.id
}

resource "azurerm_linux_virtual_machine" "p-be-vm" {
  name                = "${var.prefix}-p-be-vm"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  network_interface_ids = [azurerm_network_interface.nic-p-05.id,
    azurerm_network_interface.nic-p-44.id
  ]
  size                            = var.b-instance-type
  admin_username                  = var.username
  disable_password_authentication = true
  computer_name                   = "${var.prefix}-p-be-vm"

  os_disk {
    name                 = "${var.prefix}-p-be-vm-osdisk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  admin_ssh_key {
    username   = var.username
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.mystorage.primary_blob_endpoint
  }

  tags = {
    Name = "${var.environment}-p-be-vm"
  }
}
