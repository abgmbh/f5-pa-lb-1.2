resource "azurerm_virtual_network_peering" "main-to-prd" {
  name                      = "${var.prefix}-main-to-prd"
  resource_group_name       = azurerm_resource_group.main.name
  virtual_network_name      = azurerm_virtual_network.main.name
  remote_virtual_network_id = azurerm_virtual_network.prd.id
  allow_forwarded_traffic   = true
}

resource "azurerm_virtual_network_peering" "prd-to-main" {
  name                      = "${var.prefix}-prd-to-main"
  resource_group_name       = azurerm_resource_group.main.name
  virtual_network_name      = azurerm_virtual_network.prd.name
  remote_virtual_network_id = azurerm_virtual_network.main.id
  allow_forwarded_traffic   = true
}

resource "azurerm_virtual_network_peering" "main-to-dev" {
  name                      = "${var.prefix}-main-to-dev"
  resource_group_name       = azurerm_resource_group.main.name
  virtual_network_name      = azurerm_virtual_network.main.name
  remote_virtual_network_id = azurerm_virtual_network.dev.id
  allow_forwarded_traffic   = true
}

resource "azurerm_virtual_network_peering" "dev-to-main" {
  name                      = "${var.prefix}-dev-to-main"
  resource_group_name       = azurerm_resource_group.main.name
  virtual_network_name      = azurerm_virtual_network.dev.name
  remote_virtual_network_id = azurerm_virtual_network.main.id
  allow_forwarded_traffic   = true
}
