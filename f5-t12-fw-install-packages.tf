# Setup Onboarding scripts
data "template_file" "vm_onboard" {
  template = file("${path.module}/onboard.tpl")

  vars = {
    admin_user     = var.username
    admin_password = var.password
    DO_URL         = var.DO-URL
    AS3_URL        = var.AS3-URL
    TS_URL         = var.TS-URL
    CF_URL         = var.CF-URL
    libs_dir       = var.libs-dir
    onboard_log    = var.onboard-log
    mgmt_gw        = var.mgmt-gw
  }
}