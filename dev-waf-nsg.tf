resource "azurerm_network_security_group" "nsg-d-1424" {
  name                = "${var.prefix}-nsg-d-1424"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  security_rule {
    name                       = "allow_HTTP"
    description                = "Allow HTTP access"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefixes      = ["0.0.0.0/0"]
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow_HTTPS"
    description                = "Allow HTTPS access"
    priority                   = 120
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefixes      = ["0.0.0.0/0"]
    destination_address_prefix = "*"
  }

  tags = {
    Name        = "${var.environment}-nsg-d-1424"
  }
}

resource "azurerm_network_security_group" "nsg-d-34" {
  name                = "${var.prefix}-nsg-d-34"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  security_rule {
    name                       = "allow_all"
    description                = "allow_all"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefixes      = ["0.0.0.0/0"]
    destination_address_prefix = "*"
  }

  tags = {
    Name        = "${var.environment}-nsg-d-34"
  }
}