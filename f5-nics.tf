resource "azurerm_network_interface" "nic-04" {
  name                = "${var.prefix}-nic-04"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s0.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-04
    public_ip_address_id          = azurerm_public_ip.pip-04.id
  }

  tags = {
    Name        = "${var.environment}-nic-04"
    application = var.application
  }
}

resource "azurerm_network_interface" "nic-05" {
  name                = "${var.prefix}-nic-05"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s0.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-05
    public_ip_address_id          = azurerm_public_ip.pip-05.id
  }

  tags = {
    Name        = "${var.environment}-nic-05"
  }
}

resource "azurerm_network_interface" "nic-14" {
  name                 = "${var.prefix}-nic-14"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s1.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-14
    primary                       = true
    public_ip_address_id          = azurerm_public_ip.pip-14.id
  }

  tags = {
    Name                      = "${var.environment}-ip-14"
  }
}

resource "azurerm_network_interface" "nic-24" {
  name                 = "${var.prefix}-nic-24"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s2.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-24
    primary                       = true
    public_ip_address_id          = azurerm_public_ip.pip-24.id
  }

  tags = {
    Name                      = "${var.environment}-nic-24"
  }
}

resource "azurerm_network_interface" "nic-34" {
  name                 = "${var.prefix}-nic-34"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s3.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-34
    primary                       = true
  }

  tags = {
    Name                      = "${var.environment}-nic-34"
  }
}

resource "azurerm_network_interface" "nic-54" {
  name                 = "${var.prefix}-nic-54"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s5.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-54
    primary                       = true
  }

  tags = {
    Name                      = "${var.environment}-nic-54"
  }
}

resource "azurerm_network_interface" "nic-44" {
  name                 = "${var.prefix}-nic-44"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s4.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-44
    primary                       = true
  }

  tags = {
    Name                      = "${var.environment}-nic-44"
  }
}

resource "azurerm_network_interface" "nic-64" {
  name                 = "${var.prefix}-nic-64"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.s6.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ip-64
    primary                       = true
  }

  tags = {
    Name                      = "${var.environment}-nic-64"
  }
}

# Associate network security groups with NICs
resource "azurerm_network_interface_security_group_association" "nsg-04" {
  network_interface_id      = azurerm_network_interface.nic-04.id
  network_security_group_id = azurerm_network_security_group.nsg-0.id
}

resource "azurerm_network_interface_security_group_association" "nsg-05" {
  network_interface_id      = azurerm_network_interface.nic-05.id
  network_security_group_id = azurerm_network_security_group.nsg-0.id
}

resource "azurerm_network_interface_security_group_association" "nsg-14" {
  network_interface_id      = azurerm_network_interface.nic-14.id
  network_security_group_id = azurerm_network_security_group.nsg-1424.id
}

resource "azurerm_network_interface_security_group_association" "nsg-24" {
  network_interface_id      = azurerm_network_interface.nic-24.id
  network_security_group_id = azurerm_network_security_group.nsg-1424.id
}

resource "azurerm_network_interface_security_group_association" "nsg-34" {
  network_interface_id      = azurerm_network_interface.nic-34.id
  network_security_group_id = azurerm_network_security_group.nsg-34544464.id
}

resource "azurerm_network_interface_security_group_association" "nsg-44" {
  network_interface_id      = azurerm_network_interface.nic-44.id
  network_security_group_id = azurerm_network_security_group.nsg-34544464.id
}

resource "azurerm_network_interface_security_group_association" "nsg-54" {
  network_interface_id      = azurerm_network_interface.nic-54.id
  network_security_group_id = azurerm_network_security_group.nsg-34544464.id
}

resource "azurerm_network_interface_security_group_association" "nsg-64" {
  network_interface_id      = azurerm_network_interface.nic-64.id
  network_security_group_id = azurerm_network_security_group.nsg-34544464.id
}