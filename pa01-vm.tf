resource "azurerm_virtual_machine" "pa01-vm" {
  name                            = "${var.prefix}-pa01-vm"
  location                        = azurerm_resource_group.main.location
  resource_group_name             = azurerm_resource_group.main.name
  vm_size                         = var.FirewallVmSize

  plan {
    name = var.fwSku
    publisher = var.fwPublisher
    product = var.fwOffer
  }

  storage_image_reference {
    publisher = var.fwPublisher
    offer     = var.fwOffer
    sku       = var.fwSku
    version   = "latest"
  }

  storage_os_disk {
    name                  = "${var.prefix}-pa01-vm-osdisk"
    caching               = "ReadWrite"
    create_option         = "FromImage"
  }

  os_profile {
    computer_name  = var.FirewallVmName
    admin_username = var.adminUsername
    admin_password = var.adminPassword
  }

  primary_network_interface_id = azurerm_network_interface.nic-07.id
  network_interface_ids = [
                           azurerm_network_interface.nic-07.id,
                           azurerm_network_interface.nic-84.id,
                           azurerm_network_interface.nic-104.id,
                           azurerm_network_interface.nic-124.id,
                           azurerm_network_interface.nic-144.id
                          ]

  os_profile_linux_config {
    disable_password_authentication = false
  }
}