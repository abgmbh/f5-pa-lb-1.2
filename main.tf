provider "azurerm" {
  features {}
  client_id       = var.azure_client_id
  client_secret   = var.azure_client_secret
  subscription_id = var.azure_subscription_id
  tenant_id       = var.azure_tenant_id
}


resource "random_id" "id" {
  byte_length = 2
}

# Create a Resource Group
resource "azurerm_resource_group" "main" {
  name     = format("%s-rg", var.prefix)
  location = var.location
}

resource "azurerm_storage_account" "mystorage" {
  name                     = format("czmystorage%s", random_id.id.hex)
  resource_group_name      = azurerm_resource_group.main.name
  location                 = azurerm_resource_group.main.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment             = var.environment
    owner                   = var.owner
    group                   = var.group
    costcenter              = var.costcenter
    application             = var.application
    f5_cloud_failover_label = var.f5-cloud-failover-label
  }
}
