resource "azurerm_route_table" "udr-s3" {
  name                = "${var.prefix}-udr-s3"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  route {
    name                   = "F5-to-PA"
    address_prefix         = "10.0.1.0/24"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.7.4"
  }

  route {
    name                   = "default"
    address_prefix         = "0.0.0.0/0"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.7.4"
  }
}

resource "azurerm_route_table" "udr-s5" {
  name                = "${var.prefix}-udr-s5"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  route {
    name                   = "F5-to-PA"
    address_prefix         = "10.0.1.0/24"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.8.4"
  }

  route {
    name                   = "to-prd"
    address_prefix         = "10.1.0.0/16"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.8.4"
  }

  route {
    name                   = "default"
    address_prefix         = "0.0.0.0/0"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.8.4"
  }
}

resource "azurerm_route_table" "udr-s7" {
  name                = "${var.prefix}-udr-s7"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  route {
    name                   = "PA-to-F5"
    address_prefix         = "0.0.0.0/0"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.3.4"
  }
}

resource "azurerm_route_table" "udr-s8" {
  name                = "${var.prefix}-udr-s8"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  route {
    name                   = "PA-to-F5"
    address_prefix         = "0.0.0.0/0"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.5.4"
  }
}

resource "azurerm_route_table" "udr-s11" {
  name                = "${var.prefix}-udr-s11"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  route {
    name                   = "PA-to-WAF"
    address_prefix         = "10.0.1.0/24"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.1.1.4"
  }

  route {
    name                   = "default"
    address_prefix         = "0.0.0.0/0"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.1.1.4"
  }
}

resource "azurerm_route_table" "udr-s12" {
  name                = "${var.prefix}-udr-s12"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  route {
    name                   = "PA-to-WAF"
    address_prefix         = "10.0.1.0/24"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.1.2.4"
  }

  route {
    name                   = "to-prd"
    address_prefix         = "10.1.0.0/16"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.1.2.4"
  }

  route {
    name                   = "default"
    address_prefix         = "0.0.0.0/0"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.1.2.4"
  }

}

resource "azurerm_route_table" "udr-ps1" {
  name                = "${var.prefix}-udr-ps1"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  route {
    name                   = "WAF1-to-PA1"
    address_prefix         = "0.0.0.0/0"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.11.4"
  }
}

resource "azurerm_route_table" "udr-ps2" {
  name                = "${var.prefix}-udr-ps2"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  route {
    name                   = "WAF1-to-PA2"
    address_prefix         = "0.0.0.0/0"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.12.4"
  }
}

resource "azurerm_route_table" "udr-ps4" {
  name                = "${var.prefix}-udr-ps4"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  route {
    name                   = "BE-to-WAF"
    address_prefix         = "0.0.0.0/0"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.1.3.4"
  }

  route {
    name                   = "prd-to-dev"
    address_prefix         = "10.2.0.0/16"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.1.3.4"
  }
}

resource "azurerm_route_table" "udr-ds1" {
  name                = "${var.prefix}-udr-ds1"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  route {
    name                   = "WAF2-to-PA1"
    address_prefix         = "0.0.0.0/0"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.13.4"
  }
}

resource "azurerm_route_table" "udr-ds2" {
  name                = "${var.prefix}-udr-ds2"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  route {
    name                   = "WAF2-to-PA2"
    address_prefix         = "10.0.1.4/32"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.14.4"
  }

  route {
    name                   = "dev-to-prd"
    address_prefix         = "10.1.0.0/16"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.14.4"
  }
}

resource "azurerm_route_table" "udr-ds4" {
  name                = "${var.prefix}-udr-ds4"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  route {
    name                   = "BE-to-VIP"
    address_prefix         = "10.0.1.0/24"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.2.3.4"
  }

  route {
    name                   = "dev-to-prd"
    address_prefix         = "10.1.0.0/16"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.2.3.4"
  }
}

resource "azurerm_subnet_route_table_association" "udr-s3" {
  subnet_id      = azurerm_subnet.s3.id
  route_table_id = azurerm_route_table.udr-s3.id
}

resource "azurerm_subnet_route_table_association" "udr-s5" {
  subnet_id      = azurerm_subnet.s5.id
  route_table_id = azurerm_route_table.udr-s5.id
}

resource "azurerm_subnet_route_table_association" "udr-s7" {
  subnet_id      = azurerm_subnet.s7.id
  route_table_id = azurerm_route_table.udr-s7.id
}

resource "azurerm_subnet_route_table_association" "udr-s8" {
  subnet_id      = azurerm_subnet.s8.id
  route_table_id = azurerm_route_table.udr-s8.id
}

resource "azurerm_subnet_route_table_association" "udr-s11" {
  subnet_id      = azurerm_subnet.s11.id
  route_table_id = azurerm_route_table.udr-s11.id
}

resource "azurerm_subnet_route_table_association" "udr-s12" {
  subnet_id      = azurerm_subnet.s12.id
  route_table_id = azurerm_route_table.udr-s12.id
}

resource "azurerm_subnet_route_table_association" "udr-ps1" {
  subnet_id      = azurerm_subnet.ps1.id
  route_table_id = azurerm_route_table.udr-ps1.id
}

resource "azurerm_subnet_route_table_association" "udr-ps2" {
  subnet_id      = azurerm_subnet.ps2.id
  route_table_id = azurerm_route_table.udr-ps2.id
}

resource "azurerm_subnet_route_table_association" "udr-ps4" {
  subnet_id      = azurerm_subnet.ps4.id
  route_table_id = azurerm_route_table.udr-ps4.id
}

resource "azurerm_subnet_route_table_association" "udr-ds1" {
  subnet_id      = azurerm_subnet.ds1.id
  route_table_id = azurerm_route_table.udr-ds1.id
}

resource "azurerm_subnet_route_table_association" "udr-ds2" {
  subnet_id      = azurerm_subnet.ds2.id
  route_table_id = azurerm_route_table.udr-ds2.id
}

resource "azurerm_subnet_route_table_association" "udr-ds4" {
  subnet_id      = azurerm_subnet.ds4.id
  route_table_id = azurerm_route_table.udr-ds4.id
}
